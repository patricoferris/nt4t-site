import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'
// import Content from '../components/Content'

export const SupportPageTemplate = ({ title, supporters }) => {
  return (
    <section className="section section--gradient">
      <div className="container">
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="section">
              <h2 className="title is-size-3 has-text-weight-bold is-bold-light">
                {title}
              </h2>
              {
                supporters.map(supporter => {
                  return (
                    <div style={{marginTop: '40px'}}>
                      <h2 className='is-size-4' style={{color: '#0b3555', borderBottom: '#0b3555 thin solid'}}>{supporter.name}</h2>
                      <p style={{marginTop: '19px'}}>{supporter.description}</p>
                    </div>
                  )
                })
              }
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

SupportPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  supporters: PropTypes.array
}

const SupportPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <SupportPageTemplate
        title={frontmatter.title}
        supporters={frontmatter.supporters}
      />
    </Layout>
  )
}

SupportPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default SupportPage

export const supportPageQuery = graphql`
  query SupportPage {
    markdownRemark(frontmatter: { templateKey: { eq: "support-page" } }){
      html
      frontmatter {
        title
        supporters {
          image {
            childImageSharp {
              fluid(maxWidth: 600, quality: 64) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          name
          description
        }
      }
    }
  }
`
