---
templateKey: resource-page
title: Resources
---
You've read the headlines, you've felt the impact on your community, and you're ready to take action on your campus. Not sure where to begin? Here are a few resources that might help: 

* Start a [Petition](https://action.mijente.net/efforts/students-pledge-notechforice)
* Mijente's [Take Back Tech Workshop Guide](https://notechforice.com/poped/)
* [Ethics and Technology Reading Guide](http://bit.ly/nt4t-reading-guide), from our International Day of Student Action

For educators: 

* Example module [reading list](https://rl.talis.com/3/oxford/lists/B883ED88-8D6F-583E-3B0A-78551D177BD1.html) that includes thematic content

Above all, please get in touch with us using the "Join Us" page or via Twitter (we're @NoTech4Tyrants), and we'll connect you to as many helpful further resources as we can.
