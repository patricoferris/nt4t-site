---
templateKey: support-page
title: 'Our Work '
supporters:
  - description: >-
      Our work at Cambridge University encompasses research, advocacy, and
      public education. From carrying out investigative research into government
      ties with Palantir to organising teach-outs and campaigning for Palantir
      to be dropped as a member of the Computer Science Supporters' Club, NT4T
      at Cambridge has worked with its branch of the UCU as well as the
      Cambridge Migration Society on campus.
    image: /img/apple-icon-180x180.png
    name: Cambridge
  - description: >-
      At Oxford University, NT4T has worked to draw attention to the complicity
      of U.K. higher education institutions in the plight of migrants. Alongside
      groups like Oxford Against Schwarzman, Common Ground Oxford, and the
      Oxford Migrant Solidarity Group, NT4T at Oxford has held solidarity
      events, as well as incorporated NT4T thematic issues into module reading
      material for students. 
    image: /img/apple-icon-180x180.png
    name: Oxford
  - description: >-
      NT4T at the University of St Andrews has lobbied the university
      administration to implement ethics guidelines for corporate invitations to
      campus. NT4T at St Andrews has engaged in actions like demonstrations at
      career events, as well as educational programming with organisations like
      St Andrews Women in Computer Science.
    image: /img/apple-icon-180x180.png
    name: St Andrews
  - description: >
      At the University of Edinburgh, NT4T draws attention to the ongoing
      partnerships between Palantir and various on-campus groups. Through
      partnerships with Beneficial AI Society, NT4T at Edinburgh has
      successfully campaigned to disinvite Palantir from past career events and
      will host the upcoming NT4T Scotland Summit: Power, Migration, and Ethics
      in the Age of Surveillance. 
    image: /img/apple-icon-180x180.png
    name: Edinburgh
---

