---
templateKey: blog-post
title: 'Upcoming Event: Power, Migration, and Ethics in the Age of Surveillance'
date: 2020-03-08T16:54:03.565Z
description: >-
  NT4T is organising across the U.K. If you don't see an event near your campus,
  why not join us? 
featuredpost: true
featuredimage: /img/notech.jpg
tags:
  - whats-on
---
No Tech for Tyrants Scotland will hold a summit on Power, Migration, and Ethics in the Age of Surveillance. The summit, featuring speakers from Edinburgh, Cambridge, and St Andrews, will take place on April 3 at 14:00 in the Dugald Stewart Building, Room 1.20. Free and open to the public.
