---
templateKey: index-page
title: No Tech For Tyrants
image: /img/nt4t-round-logo.jpeg
heading: 'No Tech For Tyrants: a student-led, U.K.-wide movement against violent tech. '
subheading: >-
  We're students at universities like Oxford, Cambridge, St Andrews, and
  Edinburgh who know it's time to take a stand against the role of technology
  companies in oppressing migrant communities. We believe that students have the
  power to dismantle the tech-talent pipeline. Your voice can make a difference
  in this fight: will you join us?
mainpitch:
  title: What is NT4T?
  description: |
    A nice description of what the goal is 
description: >-
  We're students at universities like Oxford, Cambridge, St Andrews, and
  Edinburgh who know it's time to take a stand against the role of technology
  companies in oppressing migrant communities. We believe that students have the
  power to dismantle the tech-talent pipeline. Your voice can make a difference
  in this fight: will you join us?
intro:
  blurbs:
    - image: /img/mijente.jpg
      text: >-
        No Tech for Tyrants developed as a cross-border solidarity movement in
        partnership with #NoTechforICE, spearheaded by student activists and
        Mijente in the United States. You can learn more at
        http://notechforice.com.
  heading: filler
  description: filler
main:
  heading: filler
  description: filler
  image1:
    alt: fillerf
    image: /img/apple-icon-180x180.png
  image2:
    alt: filler
    image: /img/apple-icon-180x180.png
  image3:
    alt: filler
    image: /img/apple-icon-180x180.png
---

