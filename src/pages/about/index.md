---
templateKey: about-page
title: About Us
---
### No Tech for Tyrants (NT4T) is a student-led, UK-based organisation working to sever the links between higher education, violent technology, and hostile immigration environments.

### No Tech for Tyrants organises, researches, and campaigns to dismantle the violence infrastructure at the intersection of ethics, technology, migration governance, and surveillance. Some of our work includes:

* ### Coordinating student-led research highlighting the role of higher education institutions in housing and enabling violent technology companies;
* ### Equipping future and current tech workers with information they need to hold the technology industry accountable for its complicity in human suffering;
* ### Hosting teach-ins, talks, and workshops for members of the academic and activist community; and
* ### Pushing university administrations to develop ethical guidelines for corporate partnerships.

### NT4T works alongside our US-based partners, [Mijente](https://mijente.net/) and [No Tech for ICE](https://notechforice.com/). We collaborate with our local branches of the [Universities and Colleges Union (UCU)](https://www.ucu.org.uk/),  [Privacy International](https://privacyinternational.org/), and other organisations.
